<?php

final class EventType {
    const Added   = 0x00000001;
    const Deleted = 0x00000002;
    const Moved   = 0x00000003;
    const Updated = 0x00000004;

    // ensures that this class acts like an enum
    // and that it cannot be instantiated
    private function __construct(){}
}

class History
{
	public $historyId;
	public $rootId;
	public $itemId;
	public $srcVersion;
	public $timestamp;
	public $eventType;

	function __construct($historyId,$rootId,$itemId,$srcVersion,$timestamp,$eventType) {
		$this->historyId  = $historyId;
		$this->rootId     = $rootId;
		$this->itemId     = $itemId;
		$this->srcVersion = $srcVersion;
		$this->timestamp  = $timestamp->format('c');
		$this->eventType  = $eventType;
	}
};
?>
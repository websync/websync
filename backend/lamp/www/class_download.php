<?php 

class HttpDownload {

private $filename = '';
private $bufsize = 16384;

function __construct($filename) {
  $this->filename = $filename;
}

private function sendHeaders() {
  header('Content-type: application/octet-stream'); 
  header('Content-Disposition: attachment; filename="' . basename($this->filename) . '"'); 
  header('Last-Modified: ' . date('D, d M Y H:i:s \G\M\T' , filemtime($this->filename)));
  header('Content-Length: '.filesize($this->filename));
}

private function sendHttpStatus($responseCode, $title, $msg = '') {
	header("HTTP/1.0 $responseCode $title");
	header("Cache-Control: private, no-cache, must-revalidate");
	if (file_exists("errorpages/$responseCode.php")) include("errorpages/$responseCode.php");
	else include('errorpages/httpresponse.php');
	
	flush();
	exit;
}

function download() {
	file_exists($this->filename) or $this->sendHttpStatus(404, "Not Found");
  $this->sendHeaders();
  @set_time_limit(0);
  $filehandle = fopen($this->filename,'rb');
  while (!(connection_aborted() || connection_status() == 1) && (($buf = fread($filehandle , $this->bufsize)) != FALSE)) { 
    echo $buf; 
  }
  fclose($filehandle);
}

}
?>
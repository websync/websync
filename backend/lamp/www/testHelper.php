<?php
require_once('class_History.php');
require_once('class_DbConnection.php');

class TestHelper {
    /**
     * Runs some SQL
     * 
     * @param string $sql
     * @return boolean
     */
    public function testRunSql($sql) {
    	$db = new DbConnection();
      return $db->runSql($sql);
    }

    public function getHistories() {
    		return array( new History(0,'RootId','{47f3e13f-a3e4-4991-8f79-5d67555c3066}',0,new DateTime(),EventType::Added));
    }

    public function getHistory() {
    		return new History(0,'RootId','{47f3e13f-a3e4-4991-8f79-5d67555c3066}',0,new DateTime(),EventType::Added);
    }
    public function getHistorySince($lastHistoryId) {
			$db = new DbConnection();
      return $db->getHistorySince($lastHistoryId);    
    }
}
?>
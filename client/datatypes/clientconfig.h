#ifndef CLIENTCONFIG_H
#define CLIENTCONFIG_H

#include <QObject>

class ClientConfig : public QObject
{
  Q_OBJECT

  Q_PROPERTY(QString rootId READ rootId WRITE setRootId)
  Q_PROPERTY(QString rootPath READ rootPath WRITE setRootPath)
  Q_PROPERTY(QString uri READ uri WRITE setUri)
  Q_PROPERTY(QString user READ user WRITE setUser)
  Q_PROPERTY(QString password READ password WRITE setPassword)
  Q_PROPERTY(int lastHistoryId READ lastHistoryId WRITE setLastHistoryId)

public:
  ClientConfig(QObject* parent = 0) : QObject(parent) {}

  QString rootId() const { return m_rootId; }
  void setRootId(const QString& rootId){ m_rootId = rootId; }

  QString rootPath() const{ return m_rootPath; }
  void setRootPath(const QString& rootPath){ m_rootPath = rootPath; }

  QString uri() const{ return m_uri; }
  void setUri(const QString& uri){ m_uri = uri; }

  QString user() const{ return m_user; }
  void setUser(const QString& user){ m_user = user; }

  QString password() const{ return m_password; }
  void setPassword(const QString& password){ m_password = password; }

  int lastHistoryId() const{ return m_lastHistoryId; }
  void setLastHistoryId(const int lastHistoryId){ m_lastHistoryId = lastHistoryId;}

private:
  QString m_rootId;
  QString m_rootPath;
  QString m_uri;
  QString m_user;
  QString m_password;
  int m_lastHistoryId;

};

#endif // CLIENTCONFIG_H

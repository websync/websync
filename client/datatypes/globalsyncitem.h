#ifndef GLOBALSYNCITEM_H
#define GLOBALSYNCITEM_H

#include <QObject>
#include <QtCore>

class GlobalSyncItem : public QObject
{
  Q_OBJECT

  Q_PROPERTY(QString rootId READ rootId WRITE setRootId)
  Q_PROPERTY(QUuid itemId READ itemId WRITE setItemId)
  Q_PROPERTY(ItemType itemType READ itemType WRITE setItemType)
  Q_PROPERTY(QString path READ path WRITE setPath)
  Q_PROPERTY(int version READ version WRITE setVersion)
  Q_ENUMS(ItemType)

public:
  enum ItemType{File, Folder};

  GlobalSyncItem(QObject* parent = 0) : QObject(parent) {}

  QString rootId() const { return m_rootId; }
  QUuid itemId() const { return m_itemId; }
  ItemType itemType() const { return m_itemType; }
  QString path() const { return m_path; }
  int version() const { return m_version; }

  void setRootId(QString rootId) { m_rootId = rootId; }
  void setItemId(QUuid itemId) { m_itemId = itemId; }
  void setItemType(ItemType itemType) { m_itemType = itemType; }
  void setPath(QString path) { m_path = path; }
  void setVersion(int version) { m_version = version; }

private:
  QString m_rootId;
  QUuid m_itemId;
  ItemType m_itemType;
  QString m_path;
  int m_version;
};

#endif // GLOBALSYNCITEM_H

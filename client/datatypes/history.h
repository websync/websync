#ifndef HISTORY_H
#define HISTORY_H

#include <QObject>
#include <QtCore>

class History : public QObject
{
  Q_OBJECT

  Q_PROPERTY(int historyId READ historyId WRITE setHistoryId)
  Q_PROPERTY(QString rootId READ rootId WRITE setRootId)
  Q_PROPERTY(QString itemId READ getItemIdAsString WRITE setItemIdFromString)
  //Q_PROPERTY(QUuid itemId READ itemId WRITE setItemId)
  Q_PROPERTY(EventType eventType READ eventType WRITE setEventType)
  Q_PROPERTY(int srcVersion READ srcVersion WRITE setSrcVersion)
  Q_PROPERTY(QDateTime timestamp READ timestamp WRITE setTimestamp)
  Q_ENUMS(EventType)

public:
  enum EventType{Added,Deleted,Moved,Updated};

  History(QObject* parent = 0) : QObject(parent) {}
  ~History(){}

  int historyId() const { return m_historyId; }
  QString rootId() const { return m_rootId; }
  QUuid itemId() const { return m_itemId; }
  QString getItemIdAsString() const { return m_itemId.toString(); }
  EventType eventType() const { return m_eventType; }
  int srcVersion() const { return m_srcVersion; }
  QDateTime timestamp() const { return m_timestamp; }

  void setHistoryId(int historyId) { m_historyId = historyId; }
  void setRootId(const QString& rootId) { m_rootId = rootId; }
  void setItemId(const QUuid& itemId) { m_itemId = itemId; }
  void setItemIdFromString(const QString& itemId) { m_itemId = QUuid(itemId); }
  void setEventType(EventType eventType) { m_eventType = eventType; }
  void setSrcVersion(int srcVersion) { m_srcVersion = srcVersion; }
  void setTimestamp(const QDateTime& timestamp) { m_timestamp = timestamp; }

private:
  int m_historyId;
  QString m_rootId;
  QUuid m_itemId;
  EventType m_eventType;
  int m_srcVersion;
  QDateTime m_timestamp;

  public:
};

#endif // HISTORY_H

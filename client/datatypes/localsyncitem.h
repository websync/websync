#ifndef LOCALSYNCITEM_H
#define LOCALSYNCITEM_H

#include <QObject>
#include <QtCore>

class LocalSyncItem : public QObject
{
  Q_OBJECT

  Q_PROPERTY(QString rootId READ rootId WRITE setRootId)
  Q_PROPERTY(QUuid itemId READ itemId WRITE setItemId)
  Q_PROPERTY(ItemType itemType READ itemType WRITE setItemType)
  Q_PROPERTY(QString path READ path WRITE setPath)
  Q_PROPERTY(int lastSeenVersion READ lastSeenVersion WRITE setLastSeenVersion)
  Q_PROPERTY(QString lastSeenHash READ lastSeenHash WRITE setLastSeenHash)
  Q_ENUMS(ItemType)

public:
  enum ItemType{File, Folder};

  LocalSyncItem(QObject* parent = 0) : QObject(parent) {}

  QString rootId() const { return m_rootId; }
  QUuid itemId() const { return m_itemId; }
  ItemType itemType() const { return m_itemType; }
  QString path() const { return m_path; }
  int lastSeenVersion() const { return m_lastSeenVersion; }
  QString lastSeenHash() const { return m_lastSeenHash; }

  void setRootId(QString rootId) { m_rootId = rootId; }
  void setItemId(QUuid itemId) { m_itemId = itemId; }
  void setItemType(ItemType itemType) { m_itemType = itemType; }
  void setPath(QString path) { m_path = path; }
  void setLastSeenVersion(int version) { m_lastSeenVersion = version; }
  void setLastSeenHash(QString lastSeenHash) { m_lastSeenHash = lastSeenHash; }

private:
  QString m_rootId;
  QUuid m_itemId;
  ItemType m_itemType;
  QString m_path;
  int m_lastSeenVersion;
  QString m_lastSeenHash;
};

#endif // LOCALSYNCITEM_H

#include "localdb.h"
#include <QtSql>

QSqlDatabase LocalDB::db;

void LocalDB::initialize(const QString& dbfile)
{
    db = QSqlDatabase::addDatabase("QSQLITE","localDB");
    db.setDatabaseName(dbfile);
    if (db.open())
      qDebug() << "LocalDB: " << "DB opened";
    else
      qDebug() << "LocalDB: " << "ERROR: DB NOT opened";
}

ClientConfig* LocalDB::getClientConfig(const QString& rootId)
{
    QSqlQuery query(db);
    query.exec("select rootId,rootPath,uri,user,password,lastHistoryId from ClientConfig");
    if (query.next())
    {
      ClientConfig* result = new ClientConfig();
      result->setRootId(query.value(0).toString());
      result->setRootPath(query.value(1).toString());
      result->setUri(query.value(2).toString());
      result->setUser(query.value(3).toString());
      result->setPassword(query.value(4).toString());
      result->setLastHistoryId(query.value(5).toInt());
      return result;
    }
    else
      return NULL;
}

bool LocalDB::updateClientConfig(const ClientConfig* cconfig)
{
    QSqlQuery query(db);
    query.prepare("update ClientConfig set rootPath=?,uri=?,user=?,password=?,lastHistoryId=? where rootId=?");
    query.bindValue(0,cconfig->rootId(),QSql::In);
    query.bindValue(1,cconfig->uri(),QSql::In);
    query.bindValue(2,cconfig->user(),QSql::In);
    query.bindValue(3,cconfig->password(),QSql::In);
    query.bindValue(4,cconfig->lastHistoryId(),QSql::In);
    query.bindValue(5,cconfig->rootId(),QSql::In);
    if (!query.exec())
    {
      qDebug() << "Could not execute query for updateClientConfig";
      QSqlError err = query.lastError();
      qDebug() << "LocalDB: " << err.databaseText();
      qDebug() << "LocalDB: " << err.driverText();
      qDebug() << "LocalDB: " << err.text();
      qDebug() << "LocalDB: " << err.type();
      return false;
    }
    else
      return true;
}

void LocalDB::bindLocalSyncItem(QSqlQuery* query, LocalSyncItem* result)
{
  result->setItemId(query->value(0).toString());
  result->setItemType((LocalSyncItem::ItemType)query->value(1).toInt());
  result->setPath(query->value(2).toString());
  result->setRootId(query->value(3).toString());
  result->setLastSeenVersion(query->value(4).toInt());
  result->setLastSeenHash(query->value(5).toString());
}

LocalSyncItem* LocalDB::getLocalSyncItem(const QString& itemId)
{
  QSqlQuery query(db);
  query.prepare("select itemId,itemType,path,rootId,lastSeenVersion,lastSeenHash from LocalSyncItem where itemId=?");
  query.bindValue(0,itemId,QSql::In);
  if (query.exec() && query.next())
  {
    LocalSyncItem* result = new LocalSyncItem();
    bindLocalSyncItem(&query,result);
    return result;
  }
  else
    return NULL;
}

QList<LocalSyncItem*>* LocalDB::getLocalSyncItems(const QString& rootId)
{
  QSqlQuery query(db);
  query.prepare("select itemId,itemType,path,rootId,lastSeenVersion,lastSeenHash from LocalSyncItem where rootId=?");
  query.bindValue(0,rootId,QSql::In);
  if (!query.exec())
    return NULL;
  QList<LocalSyncItem*>* result = new QList<LocalSyncItem*>();
  while (query.next())
  {
    LocalSyncItem* item = new LocalSyncItem();
    bindLocalSyncItem(&query,item);
    result->append(item);
  }
  return result;
}

bool LocalDB::updateLocalSyncItem(const LocalSyncItem* item)
{
  QSqlQuery query(db);
  query.prepare("update LocalSyncItem set itemType=?,path=?,rootId=?,lastSeenVersion=?,lastSeenHash=? where itemId=?");
  query.bindValue(0,(int)item->itemType(),QSql::In);
  query.bindValue(1,item->path(),QSql::In);
  query.bindValue(2,item->rootId(),QSql::In);
  query.bindValue(3,item->lastSeenVersion(),QSql::In);
  query.bindValue(4,item->lastSeenHash(),QSql::In);
  query.bindValue(5,item->itemId().toString(),QSql::In);
  if (!query.exec())
  {
    qDebug() << "LocalDB: " << "Could not execute query for updateLocalSyncItem";
    QSqlError err = query.lastError();
    qDebug() << "LocalDB: " << err.databaseText();
    qDebug() << "LocalDB: " << err.driverText();
    qDebug() << "LocalDB: " << err.text();
    qDebug() << "LocalDB: " << err.type();
    return false;
  }
  else
    return true;
}

bool LocalDB::insertLocalSyncItem(const LocalSyncItem* item)
{
  QSqlQuery query(db);
  query.prepare("insert into LocalSyncItem (itemType,path,rootId,lastSeenVersion,lastSeenHash,itemId) values (?,?,?,?,?,?)");
  query.bindValue(0,(int)item->itemType(),QSql::In);
  query.bindValue(1,item->path(),QSql::In);
  query.bindValue(2,item->rootId(),QSql::In);
  query.bindValue(3,item->lastSeenVersion(),QSql::In);
  query.bindValue(4,item->lastSeenHash(),QSql::In);
  query.bindValue(5,item->itemId().toString(),QSql::In);
  if (!query.exec())
  {
    qDebug() << "LocalDB: " << "Could not execute query for insertLocalSyncItem";
    QSqlError err = query.lastError();
    qDebug() << "LocalDB: " << err.databaseText();
    qDebug() << "LocalDB: " << err.driverText();
    qDebug() << "LocalDB: " << err.text();
    qDebug() << "LocalDB: " << err.type();
    return false;
  }
  else
    return true;
}

bool LocalDB::deleteLocalSyncItem(const LocalSyncItem* item)
{
  QSqlQuery query(db);
  query.prepare("delete from LocalSyncItem where itemId=?");
  query.bindValue(0,item->itemId().toString(),QSql::In);
  if (!query.exec())
  {
    qDebug() << "LocalDB: " << "Could not execute query for deleteLocalSyncItem";
    QSqlError err = query.lastError();
    qDebug() << "LocalDB: " << err.databaseText();
    qDebug() << "LocalDB: " << err.driverText();
    qDebug() << "LocalDB: " << err.text();
    qDebug() << "LocalDB: " << err.type();
    return false;
  }
  else
    return true;
}

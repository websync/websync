#ifndef LOCALDB_H
#define LOCALDB_H

#include <QObject>
#include <QtSql>

#include "../datatypes/clientconfig.h"
#include "../datatypes/localsyncitem.h"

class LocalDB : public QObject
{
private:
  LocalDB(){}

  static QSqlDatabase db;
public:
  static void initialize(const QString& dbfile);
  static ClientConfig* getClientConfig(const QString& rootId);
  static bool updateClientConfig(const ClientConfig* cconfig);

  static LocalSyncItem* getLocalSyncItem(const QString& itemId);
  static QList<LocalSyncItem*>* getLocalSyncItems(const QString& rootId);
  static bool updateLocalSyncItem(const LocalSyncItem* item);
  static bool insertLocalSyncItem(const LocalSyncItem* item);
  static bool deleteLocalSyncItem(const LocalSyncItem* item);

private:
  static void bindLocalSyncItem(QSqlQuery* query, LocalSyncItem* result);


};

#endif // LOCALDB_H

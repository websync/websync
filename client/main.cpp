#include <QtCore/QCoreApplication>

#include <QtCore>
#include <QtSql>

#include "db/localdb.h"
#include "serializer.h"
#include "websync.h"
#include "datatypes/clientconfig.h"
#include "datatypes/history.h"
#include "datatypes/globalsyncitem.h"
#include "datatypes/localsyncitem.h"
#include "test/JSONCommunicationTest.h"

//QTEST_MAIN(JSONCommunicationTest)

int main(int argc, char ** argv){
  QCoreApplication app(argc,argv);

  QList<QObject*> tests;
  tests
      //<< new TestQString()
      << new JSONCommunicationTest()
      ;

  QListIterator<QObject*> testIterator(tests);
  while (testIterator.hasNext())
  {
    QObject* test = testIterator.next();
    if (0 != QTest::qExec(test))
      return -1;
  }

  //return app.exec();
}

int main_originalTest(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    LocalDB::initialize("../clientdata/clientdb.sqlite");

    ClientConfig* cc;
    cc=LocalDB::getClientConfig("testRoot");

//    qDebug() << Serializer::serialize(cc);

    WebSync syncApi;

    QList<History*>* histories = syncApi.getHistorySince(-1);

    //TODO: detect conflicts
    
    //build list of syncitems to get
    QList<History*>::iterator h;
    QStringList itemIds2update;
    for (h=histories->begin();h!=histories->end();++h)
    {
      itemIds2update.append((*h)->itemId());
    }

    //get most recent syncItems
    QList<GlobalSyncItem*>* gsis = syncApi.getSyncItems(itemIds2update);
    QMap<QUuid,GlobalSyncItem*> gsiMap;
    QList<GlobalSyncItem*>::iterator itGsi;
    for (itGsi=gsis->begin();itGsi!=gsis->end();++itGsi)
    {
      GlobalSyncItem* gsi = *itGsi;
      gsiMap.insert(gsi->itemId(),gsi);
    }

    for (h=histories->begin();h!=histories->end();++h)
    {
        bool isSuccess = false;
        History* hist=*h;
        LocalSyncItem* lsi = LocalDB::getLocalSyncItem(hist->itemId());
        GlobalSyncItem* gsi = gsiMap.value(hist->itemId());
        if (gsi == NULL)
        {
            qDebug() << "serious ERROR, found history without GlobalSyncItem: "<< hist->itemId();
        }

        switch (hist->eventType())
        {
        case History::Added:
          {
            qDebug() << "found new SyncItem: "<< hist->itemId() << " - " << gsi->path();
            if (lsi == NULL)
            {
                lsi = new LocalSyncItem();
                lsi->setItemId(hist->itemId());
                lsi->setItemType((LocalSyncItem::ItemType)gsi->itemType());
                lsi->setPath(gsi->path());
                lsi->setRootId(gsi->rootId());
            }
            QFileInfo fi;
            fi.setFile(cc->rootPath(),lsi->path());
            isSuccess = syncApi.downloadSyncItem(lsi->itemId(),fi);
            QString hash = WebSync::calculateHash(fi);
            lsi->setLastSeenHash(hash);
            lsi->setLastSeenVersion(gsi->version());
            if (isSuccess)
            {
              if (LocalDB::insertLocalSyncItem(lsi))
              {
                qDebug() << "syncitem Added: " << lsi->itemId()
                    << " | " << lsi->path()
                    << " | " << lsi->lastSeenVersion()
                    << " | " << lsi->lastSeenHash();
              }
              else
              {
                qDebug() << "could not insert syncitem into DB";
              }
            }
            else
            {
              qDebug() << "could not create syncitem";
            }
            break;
          }
        case History::Deleted:
          {
          qDebug() << "found deleted SyncItem: "<< hist->itemId() << " - " << gsi->path();
          QFileInfo fi;
          fi.setFile(cc->rootPath(),gsi->path());
          if (fi.exists())
            isSuccess = QFile::remove(fi.absoluteFilePath());
          else
            isSuccess =  true;
          if (isSuccess)
          {
            if (LocalDB::deleteLocalSyncItem(lsi))
            {
              qDebug() << "syncitem Deleted: " << lsi->itemId()
                  << " | " << lsi->path()
                  << " | " << lsi->lastSeenVersion()
                  << " | " << lsi->lastSeenHash();
            }
            else
            {
              qDebug() << "could not delete syncitem from DB";
            }
          }
          else
          {
            qDebug() << "could not delete syncitem";
          }
          break;
          }
        case History::Moved:
          {
          qDebug() << "found moved SyncItem: "<< hist->itemId() << " - " << gsi->path();
          if (lsi == NULL)
          {
            qDebug() << "ERROR: moved item not found locally";
          }

          QFileInfo oldFi;
          oldFi.setFile(cc->rootPath(),lsi->path());
          QFileInfo newFi;
          newFi.setFile(cc->rootPath(),gsi->path());

          lsi->setPath(gsi->path());
          isSuccess = QFile::rename(oldFi.absoluteFilePath(),newFi.absoluteFilePath());
          QString hash = WebSync::calculateHash(newFi);
          lsi->setLastSeenHash(hash);
          lsi->setLastSeenVersion(gsi->version());
          if (isSuccess)
          {
            if (LocalDB::updateLocalSyncItem(lsi))
            {
              qDebug() << "syncitem Moved: " << lsi->itemId()
                  << " | " << lsi->path()
                  << " | " << lsi->lastSeenVersion()
                  << " | " << lsi->lastSeenHash();
            }
            else
            {
              qDebug() << "could not update syncitem into DB";
            }
          }
          else
          {
            qDebug() << "could not move syncitem";
          }
          break;
          }
        case History::Updated:
            {
            qDebug() << "found updated SyncItem: "<< hist->itemId() << " - " << gsi->path();
            QFileInfo fi;
            fi.setFile(cc->rootPath(),gsi->path());
            isSuccess = syncApi.downloadSyncItem(lsi->itemId(),fi,true);
            QString hash = WebSync::calculateHash(fi);
            lsi->setLastSeenHash(hash);
            lsi->setLastSeenVersion(gsi->version());
            if (isSuccess)
            {
              if (LocalDB::updateLocalSyncItem(lsi))
              {
                qDebug() << "syncitem Added: " << lsi->itemId()
                    << " | " << lsi->path()
                    << " | " << lsi->lastSeenVersion()
                    << " | " << lsi->lastSeenHash();
              }
              else
              {
                qDebug() << "could not update syncitem into DB";
              }
            }
            else
            {
              qDebug() << "could not update syncitem";
            }
            break;
            }
        default:
            qDebug() << "serious ERROR, unknown history EventType for historyId: "<< hist->historyId();
            break;
        }
        if (isSuccess)
        {
          cc->setLastHistoryId(hist->historyId());
          LocalDB::updateClientConfig(cc);
        }
    }


    //testing DB
/*
    qDebug()<< LocalDB::getLocalSyncItem("{47f3e13f-a3e4-4991-8f79-5d67555c3066}");
    qDebug()<<"test";
    qDebug()<<LocalDB::getLocalSyncItems("testRoot");
*/
    return a.exec();
}

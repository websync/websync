#include "serializer.h"

#include <qjson/qobjecthelper.h>
#include <qjson/parser.h>
#include <qjson/serializer.h>

#include <QtCore>

QByteArray Serializer::serialize(const QObject* obj)
{
  QJson::Serializer ser;
  QVariant tmpVariant = QJson::QObjectHelper::qobject2qvariant(obj);
  return ser.serialize(tmpVariant);
}

bool Serializer::deSerialize(QObject* obj, const QByteArray& jsonData)
{
  QJson::Parser p;
  bool ok;
  QVariant tmpVariant = p.parse(jsonData,&ok);
  if (!ok) return false;
  QJson::QObjectHelper::qvariant2qobject(tmpVariant.toMap(), obj);
  return true;
}

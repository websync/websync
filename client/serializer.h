#ifndef SERIALIZER_H
#define SERIALIZER_H

#include <QObject>

class Serializer : public QObject
{
public:
  static QByteArray serialize(const QObject* obj);
  static bool deSerialize(QObject* obj, const QByteArray& jsonData);
  static bool deSerialize(QObject* obj, const QString& jsonData) { return deSerialize(obj,jsonData.toUtf8());}
private:
  Serializer(){}
};

#endif // SERIALIZER_H

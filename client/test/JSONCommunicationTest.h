#ifndef OTHERTEST_H
#define OTHERTEST_H

#include <QObject>
#include <QtTest/QtTest>

#include "../websyncjson.h"
#include "../datatypes/history.h"

class JSONCommunicationTest : public QObject
{
  Q_OBJECT
  private:
    WebsyncJSON* m_jsonApi;
    QUuid testUuid;
    QDateTime testTime;
  public:
    JSONCommunicationTest();
  private slots:

    void initTestCase()
    {
      m_jsonApi = new WebsyncJSON(ClientConfig());
      QSignalSpy spy(m_jsonApi,SIGNAL(testRunSqlOnServerFinished(bool)));
      QSignalSpy spyError(m_jsonApi,SIGNAL(errorOccurred(QString)));

      testUuid = QUuid::createUuid();
      testTime = QDateTime::currentDateTime();

      QString prepareDBSql;
//      prepareDBSql.append("drop database websync;");
//      prepareDBSql.append("create database websync;");
//
//      prepareDBSql.append("CREATE TABLE `websync`.`History` (");
//      prepareDBSql.append("    `historyId` INT UNSIGNED NOT NULL AUTO_INCREMENT ,");
//      prepareDBSql.append("    `rootId` VARCHAR( 255 ) NOT NULL ,");
//      prepareDBSql.append("    `itemId` CHAR( 38 ) NOT NULL ,");
//      prepareDBSql.append("    `eventType` INT NOT NULL ,");
//      prepareDBSql.append("    `srcVersion` INT NOT NULL ,");
//      prepareDBSql.append("    `timestamp` DATETIME NOT NULL ,");
//      prepareDBSql.append("    PRIMARY KEY ( `historyId` ) ");
//      prepareDBSql.append("    ) ENGINE = MYISAM ; ");

      prepareDBSql.append("delete from History; insert into History values (1,'testRootId','"+testUuid.toString()+"',1,0,'");
      prepareDBSql.append(QVariant(testTime).toString()).append("');");
      m_jsonApi->testRunSqlOnServer(prepareDBSql);

      while (spy.count() <= 0 && spyError.count() <= 0)
      {
        QTest::qWait(100);
      }
      if (spyError.count() > 0)
      {
        qDebug() << ((QList<QVariant>)spyError.takeFirst())[0].toString();
        QFAIL("RPC Error");
      }
      else
      {
        QList<QVariant> result = spy.takeFirst();
        QVERIFY(result[0].toBool());
      }
    }

    void doTest()
    {
      //qRegisterMetaType<History>("History");
      QSignalSpy spy(m_jsonApi,SIGNAL(getHistorySinceFinished(void*)));
      QSignalSpy spyError(m_jsonApi,SIGNAL(errorOccurred(QString)));

      //do call to get History
      m_jsonApi->getHistoryAsync(0);
      while (spy.count() <= 0 && spyError.count() <= 0)
      {
        QTest::qWait(100);
      }
      if (spyError.count() > 0)
      {
        qDebug() << ((QList<QVariant>)spyError.takeFirst())[0].toString();
        QFAIL("RPC Error");
      }
      else
      {
        QList<QVariant> result = spy.takeFirst();
        History* h;
        QVariant v = result.takeFirst();
        void* evtArg = qvariant_cast<void*>(v);
        h = (History*) evtArg;
        //qDebug() << v;
        QCOMPARE(h->historyId(),1);
        QCOMPARE(h->rootId(),QString("testRootId"));
        QCOMPARE(h->itemId(),testUuid);
        //QCOMPARE(h->EventType(),1);
        QCOMPARE(h->srcVersion(),0);
        QCOMPARE(h->timestamp(),testTime);
      }
    }
};

#endif // OTHERTEST_H

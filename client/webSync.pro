# -------------------------------------------------
# Project created by QtCreator 2010-01-26T12:55:32
# -------------------------------------------------
QT += network \
    sql \
    testlib
QT -= gui
TARGET = cuteSync
CONFIG += console
CONFIG -= app_bundle
TEMPLATE = app
SOURCES += main.cpp \
    websync.cpp \
    db/localdb.cpp \
    websyncjson.cpp \
    test/JSONCommunicationTest.cpp
HEADERS += websync.h \
    datatypes/globalsyncitem.h \
    datatypes/clientconfig.h \
    datatypes/history.h \
    datatypes/localsyncitem.h \
    db/localdb.h \
    websyncjson.h \
    test/JSONCommunicationTest.h
CONFIG += qxt
QXT += core \
    network

MOC_DIR = ./intermediate_build/moc
OBJECTS_DIR = ./intermediate_build/objects

#include "websync.h"

WebSync::WebSync()
{
}

QList<History*>* WebSync::getHistorySince(int lastHistoryId)
{
  History* h = new History();
  QList<History*>* result = new QList<History*>;
  /*
    //ADDED
  h->setRootId("testRoot");
  h->setEventType(History::Added);
  h->setHistoryId(1);
  h->setItemId(QUuid("{47f3e13f-a3e4-4991-8f79-5d67555c3066}"));
  h->setSrcVersion(0);
  QDate now;
  h->setTimestamp(now);
  */

  /*
  //UPDATED
  h->setRootId("testRoot");
  h->setEventType(History::Updated);
  h->setHistoryId(2);
  h->setItemId(QUuid("{47f3e13f-a3e4-4991-8f79-5d67555c3066}"));
  h->setSrcVersion(0);
  QDate now;
  h->setTimestamp(now);
  */

/*
  //MOVED
  h->setRootId("testRoot");
  h->setEventType(History::Moved);
  h->setHistoryId(3);
  h->setItemId(QUuid("{47f3e13f-a3e4-4991-8f79-5d67555c3066}"));
  h->setSrcVersion(1);
  QDate now;
  h->setTimestamp(now);
*/

  //DELETED
  h->setRootId("testRoot");
  h->setEventType(History::Deleted);
  h->setHistoryId(4);
  h->setItemId(QUuid("{47f3e13f-a3e4-4991-8f79-5d67555c3066}"));
  h->setSrcVersion(2);
  QDateTime now;
  h->setTimestamp(now);


  result->append(h);
  return result;
}

QList<GlobalSyncItem*>* WebSync::getSyncItems(const QStringList& itemIds)
{
  if (itemIds.length() != 1 && itemIds.at(0) != "{47f3e13f-a3e4-4991-8f79-5d67555c3066}")
    qDebug() << "Implement this!!!";
  GlobalSyncItem* gsi = new GlobalSyncItem();
  QList<GlobalSyncItem*>* result = new QList<GlobalSyncItem*>;
  gsi->setItemId(QUuid("{47f3e13f-a3e4-4991-8f79-5d67555c3066}"));
  gsi->setItemType(GlobalSyncItem::File);
  gsi->setRootId("testRoot");
  gsi->setPath("testFile1.txt");


  //ADDED
  //gsi->setVersion(0);

  //UPDATED
  //gsi->setVersion(1);

  //MOVED
  gsi->setVersion(1);
  gsi->setPath("testFile1_moved.txt");

  //DELETED

  result->append(gsi);
  return result;
}

QByteArray WebSync::getSyncItemData(const QString& itemId)
{
  return QString("TestContent2").toUtf8();
}


void WebSync::backupFileAsConflict(const QFileInfo& fileInfo)
{
    QFileInfo newFileInfo(fileInfo);
    while (newFileInfo.exists())
        newFileInfo.setFile(newFileInfo.absoluteFilePath()+".conflict");
    QFile::copy(fileInfo.absoluteFilePath(),newFileInfo.absoluteFilePath());
}

QString WebSync::calculateHash(const QFileInfo& fileInfo)
{
    return fileInfo.lastModified().toString();
}

bool WebSync::downloadSyncItem(const QString& itemId,const QFileInfo& fi, bool allowOverwrite)
{
  if (!allowOverwrite && fi.exists())
  {
      qDebug() << "CONFLICT: new file would overwrite existing file: " << fi.absoluteFilePath();
      WebSync::backupFileAsConflict(fi);
  }
  QByteArray data = getSyncItemData(itemId);
  QFile f(fi.absoluteFilePath());
  if (!f.open(QFile::WriteOnly))
  {
    qDebug() << "could not open File: " << f.fileName();
    return false;
  }
  if (!f.write(data))
  {
    qDebug() << "could not write to File: " << f.fileName();
    return false;
  }
  f.close();
  return true;
}

#ifndef WEBSYNC_H
#define WEBSYNC_H

#include <QObject>

#include "datatypes/history.h"
#include "datatypes/globalsyncitem.h"
#include "datatypes/localsyncitem.h"

class WebSync : public QObject
{
public:
    WebSync();

    QList<History*>* getHistorySince(int lastHistoryId);
    QList<GlobalSyncItem*>* getSyncItems(const QStringList& itemIds);
    QByteArray getSyncItemData(const QString& itemId);
    bool downloadSyncItem(const QString& itemId,const QFileInfo& path, bool allowOverwrite = false);

    static void backupFileAsConflict(const QFileInfo& fileInfo);
    static QString calculateHash(const QFileInfo& fileInfo);
};

#endif // WEBSYNC_H

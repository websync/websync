#include "websyncjson.h"

#include <QxtJSONRpcClient>
#include <QxtJSONRpcCall>
#include <QDebug>
#include <QSignalSpy>

bool qvariant2qobject(const QVariantMap& variant, QObject* object)
{
  qDebug() << variant;
  QStringList properies;
  const QMetaObject *metaobject = object->metaObject();
  int count = metaobject->propertyCount();
  for (int i=0; i<count; ++i) {
    QMetaProperty metaproperty = metaobject->property(i);
    if (metaproperty.isWritable()) {
      properies << QLatin1String( metaproperty.name());
    }
  }

  QVariantMap::const_iterator iter;
  bool success = true;
  for (iter = variant.constBegin(); success && iter != variant.end(); iter++) {
    if (properies.contains(iter.key())) {
      qDebug() << iter.key();
      qDebug() << iter.value();
      success = object->setProperty(iter.key().toAscii(), iter.value());
    }
    else
    {
      success = false;
    }
  }
  return success;
}

WebsyncJSON::WebsyncJSON(const ClientConfig& config, QObject *parent) :
    QObject(parent)
{
  this->m_client = new QxtJSONRpcClient(parent);
  if (0 == config.uri().length())
    this->m_client->setServiceUrl(QString("http://localhost:18080/websync/testSVC.php"));
  else
    this->m_client->setServiceUrl(config.uri());
  this->m_pendingCall = NULL;
  this->m_pendingOperation = WebsyncJSON::None;
}

//QList<History*>* WebsyncJSON::getHistorySince(int lastHistoryId)
//{
//  getHistoryAsync(lastHistoryId);
//  connect(this,SIGNAL(getHistorySinceFinished(void*)),this,SLOT(handleGetHistoryFinished(void*)));
//  QList<History*> result = new QList<History*>();
//  return result;
//}

void WebsyncJSON::getHistoryAsync(int lastHistoryId)
{
  QVariantList arguments;
  arguments.append(QVariant(lastHistoryId));
  doCall(WebsyncJSON::getHistory,"getHistorySince",arguments);
}

void WebsyncJSON::handleCallFinished()
{
  if (this->m_pendingCall == NULL)
  {
    qFatal("There is NO json-call pending to be handled");
  }
  else
  {
    disconnect(this->m_pendingCall,SIGNAL(finished()),this,SLOT(handleCallFinished()));
  }

  if (this->m_pendingCall->isFault())
  {
    qDebug() << "Error: " << this->m_pendingCall->error();
    emit errorOccurred("Protocol Error");
  }
  else
  {
    QVariant result = this->m_pendingCall->result();
    handleCallFinished(this->m_pendingOperation,result);
  }
  this->m_pendingCall->deleteLater();
  this->m_pendingCall = NULL;
  this->m_pendingOperation = WebsyncJSON::None;
}

void WebsyncJSON::handleCallFinished(Operation op,const QVariant& result)
{
  switch (op)
  {
  case WebsyncJSON::None:
    break;
  case WebsyncJSON::testRunSql:
    {
      bool success = result.toBool();
      emit testRunSqlOnServerFinished(success);
    }
    break;
  case WebsyncJSON::getHistory:
    {
      //HistoryList* evtArgs = new HistoryList();
      History* h = new History();
      qDebug() << "result";
      qDebug() << result;
      if (!qvariant2qobject(result.toMap(),h))
      {
          qDebug() << "Could not convert JSON response: " << result;
          emit getHistorySinceFinished(NULL);
      }
      //evtArgs->append(h);
      //qDebug() << "History: " << h;
      emit getHistorySinceFinished(h);
    }
  }
}

void WebsyncJSON::doCall(Operation op, const QString& method, QVariantList arguments)
{
  if (this->m_pendingCall != NULL || this->m_pendingOperation != WebsyncJSON::None)
  {
    qFatal("There is a json-call pending");
  }
  this->m_pendingOperation = op;
  this->m_pendingCall = this->m_client->call(method,arguments);
  connect(this->m_pendingCall,SIGNAL(finished()),this,SLOT(handleCallFinished()));
  connect(this->m_pendingCall,SIGNAL(error(QNetworkReply::NetworkError)),this,SLOT(handleCallError(QNetworkReply::NetworkError)));
}

void WebsyncJSON::handleCallError(QNetworkReply::NetworkError error)
{
    qDebug() << "Error receiving data: " << error;
    emit errorOccurred("Network Error");
}


void WebsyncJSON::testRunSqlOnServer(const QString &sql)
{
  QVariantList arguments;
  arguments.append(QVariant(sql));
  doCall(WebsyncJSON::testRunSql,"testRunSql",arguments);
}

#ifndef WEBSYNCJSON_H
#define WEBSYNCJSON_H

#include <QObject>
#include <QxtJSONRpcClient>
#include <QNetworkReply>

#include "datatypes/history.h"
#include "datatypes/clientconfig.h"

class WebsyncJSON : public QObject
{
Q_OBJECT
private:
  enum Operation
  {
    None,
    getHistory,
    testRunSql
  };

  QxtJSONRpcClient* m_client;
  QxtJSONRpcCall* m_pendingCall;
  Operation m_pendingOperation;

  void doCall(Operation op, const QString& method, QVariantList arguments);


public:
  explicit WebsyncJSON(const ClientConfig& config, QObject *parent = 0);

//  QList<History*>* getHistorySince(int lastHistoryId);
  void testRunSqlOnServer(const QString& sql);
  void getHistoryAsync(int lastHistoryId);

signals:
  void errorOccurred(QString errorMsg);
  void getHistorySinceFinished(void* hist);
  void testRunSqlOnServerFinished(bool success);
public slots:

private slots:
  void handleCallFinished();
  void handleCallError(QNetworkReply::NetworkError error);
  void handleCallFinished(Operation op,const QVariant& result);
};

#endif // WEBSYNCJSON_H
